package com.example.jcd.trexscreen;

//import android.hardware.Camera;

import android.media.MediaRecorder;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;
import java.io.IOException;

/**
 * Created by JCD on 13.12.2014.
 */
public class Rec4ac1 implements SurfaceHolder.Callback {

    private MediaRecorder mMediaRecorder;
    private File mFile;
//    private Camera mCamera;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
//    private View mToggleButton;
    private boolean mInitSuccesful;

    public Rec4ac1(SurfaceView surfaceView) {
//            super.onCreate(savedInstanceState);
//        setContentView(R.layout.recorder_view);

        // we shall take the video in landscape orientation
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

//        mSurfaceView = (SurfaceView) findViewById(R.id.surface_view);
        mSurfaceView = surfaceView;
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

//        mToggleButton = (ToggleButton) findViewById(R.id.toggleRecordingButton);
//        mToggleButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            // toggle video recording
//            public void onClick(View v) {
//                if (((ToggleButton) v).isChecked()) {
//                } else {
//                }
//            }
//        });
    }

    void start() {
        mMediaRecorder.start();
//        try {
//            Thread.sleep(10 * 1000); // This will recode for 10 seconds, if you don't want then just remove it.
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        finish();
    }

    void stop() {
//        mMediaRecorder.stop();
//        mMediaRecorder.reset();
//        try {
//            initRecorder(mHolder.getSurface());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try {
            mMediaRecorder.stop();
        } catch(RuntimeException e) {
            mFile.delete();  //you must delete the outputfile when the recorder stop failed.
        } finally {
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
    }

    /* Init the MediaRecorder, the order the methods are called is vital to
     * its correct functioning */
    private void initRecorder(Surface surface) throws IOException {
        // It is very important to unlock the camera before doing setCamera
        // or it will results in a black preview
//        if (mCamera == null) {
//            mCamera = Camera.open();
//            mCamera.unlock();
//        }

        if (mMediaRecorder == null) mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setPreviewDisplay(surface);
//        mMediaRecorder.setCamera(mCamera);

        //
//        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
//        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC); // TODO
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT); // TODO
        //       mMediaRecorder.setOutputFormat(8);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT); // TODO

        mMediaRecorder.setVideoEncodingBitRate(512 * 1000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(640, 480);
        final String VIDEO_PATH_NAME = "/mnt/sdcard/VGA_30fps_512vbrate.mp4";
        mFile = new File(VIDEO_PATH_NAME);
        mMediaRecorder.setOutputFile(VIDEO_PATH_NAME);

        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            // This is thrown if the previous calls are not called with the
            // proper order
            e.printStackTrace();
        }

        mInitSuccesful = true;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (!mInitSuccesful)
                initRecorder(mHolder.getSurface());
            start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stop();
        shutdown();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    private void shutdown() {
        // Release MediaRecorder and especially the Camera as it's a shared
        // object that can be used by other applications
        mMediaRecorder.reset();
        mMediaRecorder.release();
//        mCamera.release();

        // once the objects have been released they can't be reused
        mMediaRecorder = null;
//        mCamera = null;
    }
}
